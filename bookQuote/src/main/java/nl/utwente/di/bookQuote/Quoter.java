package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> hashMAp;

    public Quoter(){
        hashMAp = new HashMap<>();
        hashMAp.put("1" , 10.0);
        hashMAp.put("2" , 45.0);
        hashMAp.put("3" , 20.0);
        hashMAp.put("4" , 35.0);
        hashMAp.put("5" , 50.0);

    }
    public double getBookPrice(String s) {
        for (String string : hashMAp.keySet()){
            if (string.equals(s)){
                return hashMAp.get(string);
            }
        }
        return 0;
    }
}